﻿namespace TechTest.Models
{
    public enum EnumStatusVenda
    {
        // [Description("Aguardando Pagamento")]
        Aguardando = 0,
        // [Description("Pagamento Aprovado")]
        Aprovado = 1,
        // [Description("Cancelado")]
        Cancelado = 2,
        // [Description("Enviado para transportadora")]
        Enviado = 3,
        // [Description("Entregue")]
        Entregue = 4
    }
}