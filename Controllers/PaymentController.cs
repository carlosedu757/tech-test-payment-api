﻿using Microsoft.AspNetCore.Mvc;
using TechTest.Context;
using TechTest.Models;

namespace TechTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentContext _context;

        public PaymentController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("Venda")]
        public IActionResult RegisterSale(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            venda.Status = 0;

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { id = venda.Id }, venda);
        }

        [HttpPost("Vendedor")]
        public IActionResult RegisterSalesPerson(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            var vendedorBanco = _context.Vendedores.Find(venda.VendedorId);

            if (vendaBanco == null)
                return NotFound();

            if(vendedorBanco == null)
                return BadRequest(new { Erro = "Vendedor não encontrado" });

            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            vendaBanco.VendedorId = venda.VendedorId;
            vendaBanco.Data = venda.Data;
            vendaBanco.Itens = venda.Itens;

            if((vendaBanco.Status == EnumStatusVenda.Aguardando) && ((venda.Status == EnumStatusVenda.Aprovado) || (venda.Status == EnumStatusVenda.Cancelado) || (venda.Status == EnumStatusVenda.Aguardando)))
                vendaBanco.Status = venda.Status;
            else if((vendaBanco.Status == EnumStatusVenda.Aprovado) && ((venda.Status == EnumStatusVenda.Enviado) || (venda.Status == EnumStatusVenda.Cancelado) || (venda.Status == EnumStatusVenda.Aprovado)))
                vendaBanco.Status = venda.Status;
            else if ((vendaBanco.Status == EnumStatusVenda.Enviado) && ((venda.Status == EnumStatusVenda.Entregue) || (venda.Status == EnumStatusVenda.Enviado)))
                vendaBanco.Status = venda.Status;
            else
                return BadRequest(new { Erro = $"Não é permitido passar a venda de status {vendaBanco.Status} para {Enum.GetName(typeof(EnumStatusVenda), venda.Status)}" });

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }
    }
}
