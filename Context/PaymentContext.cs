﻿using Microsoft.EntityFrameworkCore;
using TechTest.Models;

namespace TechTest.Context
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

        public DbSet<Venda> Vendas { get; set; }

        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
                .HasOne(s => s.Vendedor)
                .WithMany(v => v.Vendas)
                .HasForeignKey(s => s.VendedorId)
                .HasPrincipalKey(v => v.Id);
        }
    }
}

